"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this script is for performing actions for the hub
*
"""
import os
import subprocess


def osettingsmgr():
    """#Settings Manager"""
    subprocess.Popen("xfce4-settings-manager")


def odeconf():
    """#Deconf Editor"""
    subprocess.Popen("dconf-editor")


def omt():
    """#Mouse Settings"""
    subprocess.Popen("xfce4-mouse-settings")


def oclf():
    """#Appearence"""
    subprocess.Popen("xfce4-appearance-settings")


def opp():
    """#Panel Preferences"""
    subprocess.Popen(["xfce4-panel", "--preferences"])


def ons():
    """#Notifications Configuration"""
    subprocess.Popen("xfce4-notifyd-config")


def opa():
    """#Default  settings"""
    subprocess.Popen("xfce4-mime-settings")


def oks():
    """#Keyboardsettings"""
    subprocess.Popen("xfce4-keyboard-settings")


def nhnm():
    """#Network Manager"""
    subprocess.Popen("nm-connection-editor")


def nhpr():
    """#Printers"""
    subprocess.Popen("system-config-printer")


def nhdis():
    """#Display"""
    subprocess.Popen("xfce4-display-settings")


def nhpom():
    """#Power Manager"""
    subprocess.Popen("xfce4-power-manager-settings")


def nhpav():
    """#Pulse Audio Volume"""
    subprocess.Popen("pavucontrol")


def ssug():
    """#User and  Groups"""
    subprocess.Popen("users-admin")


def ssdu():
    """#Disk Utilities"""
    subprocess.Popen("gnome-disks")


def ssum():
    """#Update Manager"""
    subprocess.Popen(["pkexec", "python3", "/opt/pypep/pepu.py"])


def ssas():
    """#Accessibilitity"""
    subprocess.Popen("xfce4-accessibility-settings")


def sssi():
    """#System Informaion"""
    cmd = "xfce4-terminal -e 'bash -c \"neofetch\";bash'"
    subprocess.Popen(cmd, shell=True)


def ssfh():
    """#Flathub"""
    os.system('python3 /opt/pypep/fh.py')


def ssss():
    """#Snapstore"""
    os.system('python3 /opt/pypep/web_spstore.py')


def ssst():
    """#Snapstore"""
    cmd = "[ -e /snap/bin ] && snap-store || xfce4-terminal -T \"Snap Store Installer\" -e \"bash -c 'sudo snap install snap-store'\""
    subprocess.Popen(cmd, shell=True)


def ssai():
    """#App Image"""
    os.system('python3 /opt/pypep/web_aih.py')


def ssgn():
    """#gnomestore"""
    subprocess.Popen("gnome-software")


def ssgns():
    """#gnomestoreweb"""
    os.system('python3 /opt/pypep/web_gstore.py')


def ssspm():
    """#Synaptic Package Manage"""
    subprocess.Popen("synaptic-pkexec")


def packages():
    """#Installer"""
    subprocess.Popen(["pkexec", "python3", "/opt/pypep/peppackages.py"])


def lkumo():
    """#Launch kumo"""
    subprocess.Popen(["pkexec", "python3", "/opt/pypep/pepkumo.py"])


def lttkcreate():
    """#Launch TTK Creator"""
    subprocess.Popen(
        ["pkexec", "python3", "/usr/local/lib/python3.9/dist-packages/ttkcreator/__main__.py"])


def lpscope():
    """#Launch PepScope"""
    subprocess.Popen(["python3", "/opt/pypep/rdata.py"])
