"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This script is used to help get the wallpapers
"""

import os
import tkinter as tk
from tendo import singleton
import ttkbootstrap as ttk

# setting up window
pexs = ttk.Window(themename="darkly")
window_height = 120
window_width = 420
pexs.title('Additional Wallpaper')
pexs.tk.call('wm', 'iconphoto', pexs._w, tk.PhotoImage(
    file='/usr/share/pixmaps/peppermint-old.png'))

# keep only instance at a time
me = singleton.SingleInstance()


def center_screen():
    """ gets the coordinates of the center of the screen """
    global screen_height, screen_width, x_cordinate, y_cordinate

    screen_width = pexs.winfo_screenwidth()
    screen_height = pexs.winfo_screenheight()
    # Coordinates of the upper left corner of the window to make the window
    # appear in the center
    x_cordinate = int((screen_width / 2) - (window_width / 2))
    y_cordinate = int((screen_height / 2) - (window_height / 2))
    pexs.geometry("{}x{}+{}+{}".format(window_width,
                  window_height, x_cordinate, y_cordinate))


def wls():
    """Check if user can connect to codeberg"""
    pexs.destroy()
    os.system('python3 /opt/pypep/diagwlschk.py')


def xc():
    """close the form  start back at pge"""
    pexs.destroy()
    os.popen('python3 /opt/pypep/pge.py')


# title message
lblexp = ttk.Label(
    pexs,
    bootstyle="light",
    text="Would you like to install addtional wallpaper created by the Peppermint Team and Community?.",
    wraplength=400)
lblexp.pack(side="top")


###All the buttons and placements##
btncore = ttk.Button(
    text="Install",
    bootstyle="danger-outline",
    command=wls,
    width=8)
btncore.pack(side="left", padx=5, pady=5)
bttut = ttk.Button(
    text='Cancel',
    bootstyle="danger-outline",
    width=8,
    command=xc)
bttut.pack(side="left", padx=5, pady=5)

# call Center screen
center_screen()

pexs.mainloop()
