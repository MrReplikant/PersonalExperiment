"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This file is used to keep functions that are used for the Welcome
* Screen
"""
import os.path


def suggested_packages():
    """ Open the suggested packages """
    os.system('pkexec python3 /opt/pypep/peppackages.py')


def pep_extras():
    """Open peppemrinmt Extra's"""
    os.system('python3 /opt/pypep/pge.py')


def hub():
    """Open the Hub"""
    os.system('python3 /opt/pypep/hub.py &')


##SSB's###
def kumo_docs():
    """Open the Kumo docs"""
    os.system('python3 /opt/pypep/web_tutorial.py &')


def pep_docs():
    """Open Pep Docs"""
    os.system('python3 /opt/pypep/web_pdocs.py &')

# Community Section


def twitter():
    """Open the Pep twitter"""
    os.system('python3 /opt/pypep/web_twtr.py &')


def reddit():
    """Open Reddit"""
    os.system('python3 /opt/pypep/web_reddit.py &')


def mastodon():
    """Open Mastodon"""
    os.system('python3 /opt/pypep/web_mas.py &')


def code_berg():
    """Open Codeberg"""
    os.system('python3 /opt/pypep/web_codeb.py &')


def matrix():
    """Open Matrix"""
    os.system('python3 /opt/pypep/web_mat.py &')


def source_forge():
    """Open Sourceforge"""
    os.system('python3 /opt/pypep/web_sf.py &')


def build_date():
    """Open BuildDate wiki"""
    os.system('python3 /opt/pypep/web_bd.py &')
