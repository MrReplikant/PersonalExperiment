#!/bin/bash

_extras=pepicons
_repo="Peppermint_OS/PepIcons"

###########################################
# Quietly have wget -q0- "pipe" its output directly to tar.
# Use tar -xvz to decompress, list and untar the DL file
# to the symlink 'pepicons' in /usr/share, on the fly.
# Leaving no files to remove and no permission changes required.
###########################################

echo -e "\n Downloadng Extended Icon set to /usr/share/"
wget https://codeberg.org/${_repo}/archive/main.tar.gz --show-progress -qO -|
  tar -xz -C /usr/share 2>/dev/null

echo -e "\n Extended Icon set downloded and installed\n\n Updating system icon caches."
echo -en "\n\t Password required to update icon cache\n\t "

sudo /usr/sbin/update-icon-caches /usr/share/icons/* 
echo
read -n1 -p " Process Completed. Press any key to close this dialog." answ

# Go back to the Extras
python3 /opt/pypep/pge.py &

